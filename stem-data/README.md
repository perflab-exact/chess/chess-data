<!-- -*-Mode: markdown;-*- -->
<!-- $Id$ -->

PNNL's [CHESS](https://gitlab.com/perflab-exact/chess) (Cloud, HPC, and Edge for Science and Security): Materials thrust
=============================================================================

**Home**:
  - https://gitlab.com/perflab-exact/chess


**About**: 

Dataset release: Spurgeon S.R., C.M. Doty, J.F. Strube, W.M. Abebe, L. Guo, N.R. Tallent, and O. Bel, et al. 2024. "Images and Labels supporting the ICML submission." PNNL-SA-194952.

**Related**:


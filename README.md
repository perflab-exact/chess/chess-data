<!-- -*-Mode: markdown;-*- -->
<!-- $Id$ -->

PNNL's [CHESS](https://gitlab.com/perflab-exact/chess) (Cloud, HPC, and Edge for Science and Security): Materials thrust
=============================================================================

**Home**:
  - https://gitlab.com/perflab-exact/chess


**About**: CHESS seeks to demonstrate that novel hybrid computational architectures that include cloud, high-performance computing, and edge can enable impactful science and mission goals. The cloud and HPC combination will be researched to demonstrate foundational scientific workflows whereas the cloud and edge combination will demonstrate national security mission workflows. Additionally, CHESS will coordinate with establishing foundational cloud technical capabilities for the laboratory.


**Related**:

  - AutoEM/CHESS:
  https://gitlab.pnnl.gov/joecool/chess

  - AutoEM, labeled data:
    https://gitlab.pnnl.gov/joecool/few-shot-electron-microscopy/-/tree/script_cluster/STO_GE_ground_truth, branch "develop2"

  - AutoEM, unlabeled data (Data Hub):
    https://data.pnl.gov/group/125/nodes/dataset/34570

  - JoeCool (general): https://gitlab.pnnl.gov/joecool


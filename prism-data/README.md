<!-- -*-Mode: markdown;-*- -->
<!-- $Id$ -->


Sample prism data is located here:
- H5 directory: `/qfs/projects/chess/crom273/prism_output_files`
- Image directory: `/qfs/projects/chess/crom273/prism_output_images`
 
The subdirectorys `mp-[ID]` correspond to the Material Projects ID, e.g, mp-5229 is SrTiO3 (strontium titanite).
